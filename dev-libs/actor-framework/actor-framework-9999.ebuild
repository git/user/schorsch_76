# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit git-2 cmake-utils flag-o-matic

EGIT_REPO_URI="https://github.com/actor-framework/actor-framework"
EGIT_BRANCH="master"

DESCRIPTION="An Open Source Implementation of the Actor Model in C++"
HOMEPAGE="https://actor-framework.org/"

LICENSE="Boost-1.0"

SLOT="0"

IUSE=""

RDEPEND="
	>=dev-libs/protobuf-2.5.0
	dev-libs/libpthread-stubs
	dev-util/cmake
	>=sys-devel/gcc-4.8.0
	>=net-misc/curl-7.45.0"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

KEYWORDS="~amd64"

src_configure()
{
	append-cxxflags -std=c++11 -Wextra -Wall -pedantic -ftemplate-depth=512 -pthread -fPIC -Wno-deprecated-declarations
	cmake-utils_src_configure
}
