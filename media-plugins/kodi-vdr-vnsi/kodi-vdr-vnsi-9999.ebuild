# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit git-2 cmake-utils flag-o-matic

EGIT_REPO_URI="https://github.com/kodi-pvr/pvr.vdr.vnsi.git"
EGIT_BRANCH="Jarvis"

DESCRIPTION="VDR VNSI backend for kodi"
HOMEPAGE="https://github.com/kodi-pvr/pvr.vdr.vnsi.git"

LICENSE="Apache-2.0"

SLOT="0"

IUSE=""

RDEPEND="
	>=media-libs/kodi-platform-16_p20150805
	"
DEPEND="${RDEPEND}
	dev-util/cmake
	virtual/pkgconfig"

KEYWORDS="~amd64"

src_configure()
{
	local mycmakeargs=(
		-DADDONS_TO_BUILD=pvr.vdr.vnsi
		-DCMAKE_BUILD_TYPE=Release
		-DADDON_SRC_PREFIX="$WORKDIR/$PF/src/"
		-DCMAKE_INSTALL_LIBDIR="$PREFIX/lib/kodi/"
	)
	cmake-utils_src_configure
}
