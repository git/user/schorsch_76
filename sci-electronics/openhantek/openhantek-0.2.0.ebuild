# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

EAPI="4"

inherit qt4-r2 eutils

DESCRIPTION="Digital Storage Osziloscope"
HOMEPAGE="http://www.openhantek.org/"
SRC_URI="http://downloads.sourceforge.net/project/openhantek/openhantek/${P}.tar.bz2"

LICENSE="GPL-3"

SLOT="0"

IUSE=""

DEPEND="sys-devel/gcc
	sci-libs/fftw
	dev-libs/libusb
	dev-qt/qtopengl
	dev-qt/qtgui
	dev-qt/qtcore"
RDEPEND="${DEPEND} sci-electronics/electronics-menu"

KEYWORDS="~amd64"

S="${WORKDIR}"/${PN}

src_configure() {
    export PREFIX=/usr
    qt4-r2_src_configure
}

src_install() {
    newicon res/images/openhantek.png openhantek.png
    make_desktop_entry openhantek OpenHantek openhantek Electronics
	dodoc README
    qt4-r2_src_install
}

pkg_postinst() {
	echo
	elog "You will need the firmware for your DSO installed."
	elog "Visit http://www.openhantek.org/ for futher configuration instructions."
	echo
}
